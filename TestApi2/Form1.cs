﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace TestApi2 {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            //TestSendApi();

            //SendApi3();

            //ObjectToJson();

            //TestJson();
            TestJsonRead();

            //1
        }
        private string url;
        private string json;
        private HttpWebRequest httpSend { get; set; }
        private HttpWebResponse httpRead { get; set; }
        private void TestSendApi() {
            url = string.Empty;
            //string URL = "http://postman-echo.com/post?referenceId=022078925508&productCode=001002461285&quantity=1&version=V1&signature&applicationCode=52e7cf966b724749a7c4efadc3727ed7";
            string URL = "http://ip.jsontest.com/";
            string urlList = "";

            url = URL + urlList;

            JsonClass jsonClass = new JsonClass();
            jsonClass.property = "Sites";
            jsonClass.report_type = "ALL";
            string jsonRest = new JavaScriptSerializer().Serialize(jsonClass);

            //jsonRest = "";

            send_rest(jsonRest);

            read_rest();

            string resultRest = get_result_rest();
            textBox1.Text = json;
        }
        private string gen_json_rest() {
            return new JavaScriptSerializer().Serialize(new {
                //referenceId = "022078925508",
                //productCode = "001002461285",
                //quantity = "1",
                //version = "V1",
                //signature = "",
                //applicationCode = "52e7cf966b724749a7c4efadc3727ed7",

                property = "Sites",
                report_type = "ALL",
            });
        }
        private void send_rest(string jsonRest) {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            httpSend = (HttpWebRequest)WebRequest.Create(url);
            httpSend.Method = "POST";
            httpSend.ContentType = "application/json";
            try {
                StreamWriter swJSONPayload = new StreamWriter(httpSend.GetRequestStream());
                swJSONPayload.Write(jsonRest);
                swJSONPayload.Close();
            } catch { }
        }
        private void read_rest() {
            httpRead = null;
            try {
                httpRead = (HttpWebResponse)httpSend.GetResponse();
                Stream stream = httpRead.GetResponseStream();
                if (stream != null) {
                    StreamReader reader = new StreamReader(stream);
                    json = reader.ReadToEnd();
                }
            } catch (Exception ex) {
                json = "{\"errorMessages\":\"" + ex.Message.ToString() + "\"}";
            }
        }
        private string get_result_rest() {
            List<string> values = new List<string>();
            List<string> keys = new List<string>();
            string pattern = @"\""(?<key>[^\""]+)\""\:\""?(?<value>[^\"",}]+)\""?\,?";
            foreach (Match m in Regex.Matches(json, pattern)) {
                if (m.Success) {
                    values.Add(m.Groups["value"].Value);
                    keys.Add(m.Groups["key"].Value);
                }
            }

            return string.Join(",", values);
        }

        private void SendApi() {
            // create a request
            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(url); request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "POST";

            // turn our request string into a byte stream
            string jsonRest = gen_json_rest();
            byte[] postBytes = Encoding.UTF8.GetBytes(jsonRest);

            // this is important - make sure you specify type this way
            request.ContentType = "application/json; charset=UTF-8";
            request.Accept = "application/json";
            request.ContentLength = postBytes.Length;
            //request.CookieContainer = CookieContainer
            //request.UserAgent = "apod";
            Stream requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream())) {
                result = rdr.ReadToEnd();
            }

            textBox1.Text = result;
        }

        private void SendApi2() {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.nasa.gov/planetary/apod");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
                string json = "{\"date\":\"2020-03-10\"," +
                              "\"hd\":\"true\"," +
                              "\"api_key\":\"DEMO_KEY\"}";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                var result = streamReader.ReadToEnd();

                textBox1.Text = result;
            }
        }

        private void SendApi3() {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.nasa.gov/planetary/apod");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
                string json_ = new JavaScriptSerializer().Serialize(new {
                    date = "2020-03-10",
                    hd = "true",
                    api_key = "DEMO_KEY"
                });

                streamWriter.Write(json_);
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                var result = streamReader.ReadToEnd();

                textBox1.Text = result;
            }
        }


        private void ObjectToJson() {
            Lad obj = new Lad {};
            obj.firstName = "Markoff";
            obj.lastName = "Chaney";
            obj.dateOfBirth.year = 1901;
            obj.dateOfBirth.month = 4;
            obj.dateOfBirth.day = 30;

            string json = new JavaScriptSerializer().Serialize(obj);
            textBox1.Text = json;
        }
        private void JsonToObject() {
            //Lad result = JsonConvert.DeserializeObject<Lad>(textBox1.Text);
            TestMain result = JsonConvert.DeserializeObject<TestMain>(textBox1.Text);
        }
        public class MyDate {
            public int year;
            public int month;
            public int day;
        }
        public class Lad {
            public string firstName;
            public string lastName;
            public MyDate dateOfBirth;

            public Lad() {
                dateOfBirth = new MyDate();
            }
        }
        public class TestMain {
            public string Customer { get; set; }
            public List<JsonConvert> data { get; set; }

            public TestMain() {
                data = new List<JsonConvert>();
            }
            public class JsonConvert {
                public string Date { get; set; }
                public string Time { get; set; }
                public string LoginID { get; set; }
                public string SWVersion { get; set; }
                public string FWVersion { get; set; }
                public string SpecVersion { get; set; }
                public string TestTime { get; set; }
                public string LoadInOut { get; set; }
                public string Mode { get; set; }
                public string FinalResult { get; set; }
                public string SN { get; set; }
                public object Failure { get; set; }
                public List<ResultString_> ResultString { get; set; }

                public JsonConvert() {
                    Date = string.Empty;
                    Time = string.Empty;
                    LoginID = string.Empty;
                    SWVersion = string.Empty;
                    FWVersion = string.Empty;
                    SpecVersion = string.Empty;
                    TestTime = string.Empty;
                    LoadInOut = string.Empty;
                    Mode = string.Empty;
                    FinalResult = string.Empty;
                    SN = string.Empty;
                    Failure = string.Empty;
                    ResultString = new List<ResultString_>();
                }
                public class ResultString_ {
                    public string Step { get; set; }
                    public string Description { get; set; }
                    public string Tolerance { get; set; }
                    public string Measured { get; set; }
                    public string Result { get; set; }

                    public ResultString_() {
                        Step = string.Empty;
                        Description = string.Empty;
                        Tolerance = string.Empty;
                        Measured = string.Empty;
                        Result = string.Empty;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            JsonToObject();
        }


        public class JsonClass {
            public string property { get; set; }
            public string report_type { get; set; }
        }



        private void TestJson() {
            JsonServer jsonServer = new JsonServer();
            jsonServer.Cmd = "Test";
            jsonServer.MessageID = "Test";
            jsonServer.TimeStamp = "Test";
            jsonServer.SourceID = "Test";
            jsonServer.TargetID = "Test";
            jsonServer.PayLoad.Data = "Test";
            jsonServer.PayLoad.Data1 = "Test";
            jsonServer.PayLoad.Data2 = "Test";
            jsonServer.Position.X = "Test";
            jsonServer.Position.Y = "Test";
            jsonServer.Position.YW = "Test";
            jsonServer.Battery = "Test";

            string json = new JavaScriptSerializer().Serialize(jsonServer);
            textBox1.Text = json;
        }
        private void TestJsonRead() {
            JsonServer result = JsonConvert.DeserializeObject<JsonServer>(textBox1.Text);
        }
        public class JsonServer {
            public string TimeStamp { get; set; }
            public string MessageID { get; set; }
            public string SourceID { get; set; }
            public string TargetID { get; set; }
            public string Cmd { get; set; }
            public PAYLOAD PayLoad { get; set; }
            public POSITION Position { get; set; }
            public string Battery { get; set; }

            public JsonServer() {
                TimeStamp = string.Empty;
                MessageID = string.Empty;
                SourceID = string.Empty;
                TargetID = string.Empty;
                Cmd = string.Empty;
                PayLoad = new PAYLOAD();
                Position = new POSITION();
                Battery = string.Empty;
            }
            public class PAYLOAD {
                public string Data { get; set; }
                public string Data1 { get; set; }
                public string Data2 { get; set; }

                public PAYLOAD() {
                    Data = string.Empty;
                    Data1 = string.Empty;
                    Data2 = string.Empty;
                }
            }
            public class POSITION {
                public string X { get; set; }
                public string Y { get; set; }
                public string YW { get; set; }

                public POSITION() {
                    X = string.Empty;
                    Y = string.Empty;
                    YW = string.Empty;
                }
            }
        }
    }
}
